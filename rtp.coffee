dgram = require "dgram"
path = require "path"
mjpeg = require "./mjpeg"
rtp = () ->
  fileHandler = {
    ".mjpeg": mjpeg
  }
  {
    Stream: (sourceIdentifier, filePath, port, address) ->
      header = new Buffer 12
      header[0] = 0x80 # Version: 10; Padding: 0; Extension: 0; CC: 0000
      header[1] = 0x1A # Marker: 0; Payload Type: 0011010
      header.write Math.round(100000 + Math.random() * 899999).toString(), 2 # Sequence Number: 2 random integers; Timestamp: 4 random integers
      header.write sourceIdentifier, 8 # Source Identifier: 4 provided bytes
      socket = dgram.createSocket "udp4"
      container = fileHandler[path.extname filePath].Container path.basename filePath
      timeoutId = 0
      {
        play: () ->
          interval = 40
          sendNextFrame = () ->
            if container.hasNextFrame()
              frame = container.nextFrame()
              sequenceNumber = parseInt header.toString("utf8", 2, 4), 10
              header.write (sequenceNumber + 1).toString(), 2
              timestamp = parseInt header.toString("utf8", 4, 8), 10
              header.write (timestamp + interval).toString(), 4
              packet = Buffer.concat [header, frame]
              socket.send packet, 0, packet.length, port, address
              timeoutId = setTimeout sendNextFrame, interval
            else
              container.close()
          sendNextFrame()
        pause: () ->
          clearTimeout timeoutId
        teardown: () ->
          container.close()
      }
  }
module.exports = rtp()