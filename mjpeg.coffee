fs = require "fs"
mjpeg = () ->
  {
    Container: (filePath) ->
      fileDescriptor = fs.openSync filePath, "r"
      fileSize = fs.statSync(filePath).size
      position = 0
      hasNextFrame = () ->
        position < fileSize
      {
        hasNextFrame: hasNextFrame
        nextFrame: () ->
          if hasNextFrame()
            frameLengthBuffer = new Buffer 5
            position += fs.readSync fileDescriptor, frameLengthBuffer, 0, frameLengthBuffer.length
            frameLength = parseInt frameLengthBuffer, 10
            frameBuffer = new Buffer frameLength
            position += fs.readSync fileDescriptor, frameBuffer, 0, frameBuffer.length
            frameBuffer
        close: () ->
          fs.closeSync fileDescriptor
      }
  }
module.exports = mjpeg()