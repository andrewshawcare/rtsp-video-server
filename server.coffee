net = require "net"
rtsp = require "./rtsp"
rtp = require "./rtp"
sessions = {}
net.createServer (connection) ->
  connection.setEncoding rtsp.encoding
  connection.on "data", (data) ->
    request = rtsp.Request data
    if request.method is rtsp.methods.setup
      session = rtsp.Session rtp.Stream "0000", request.uri.path, request.header.client.port, request.uri.hostname
      sessions[session.id] = session
    else
      session = sessions[request.header.session.id]
    if request.method is rtsp.methods.play
      session.stream.play()
    if request.method is rtsp.methods.pause
      session.stream.pause()
    if request.method is rtsp.methods.teardown
      session.stream.teardown()
    connection.write rtsp.Response rtsp.versions["1.0"], rtsp.statuses.ok, request.cSeq, {id: session.id}
.listen 3000