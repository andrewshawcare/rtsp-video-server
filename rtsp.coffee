url = require "url"
rtsp = () ->
  methods = {
    setup: "SETUP",
    play: "PLAY",
    pause: "PAUSE",
    teardown: "TEARDOWN"
  }
  {
    encoding: "ascii",
    methods: methods,
    versions: {
      "1.0": "RTSP/1.0"
    }
    statuses: {
      ok: {
        number: 200,
        code: "OK"
      }
    }
    Request: (text) ->
      [requestLine, cSeqLine, headerLine] = text.split "\r\n"
      [method, uriString, version] = requestLine.split " "
      uri = url.parse uriString
      cSeq = parseInt cSeqLine.match(/CSeq:\s*(\d+)/)[1], 10
      if method is methods.setup
        header = {
          transport: headerLine.match(/Transport:\s*(\w+\/\w+)/)[1],
          client: {
            port: parseInt headerLine.match(/client_port=\s*(\d+)/)[1], 10
          }
        }
      else
        header = {
          session: {
            id: headerLine.match(/Session:\s*(\w+)/)[1]
          }
        }
      {
        method: method,
        uri: uri,
        version: version,
        cSeq: cSeq,
        header: header
      }
    Response: (version, status, cSeq, session) ->
      version + " " + status.number + " " + status.code + "\r\n" +
      "CSeq: " + cSeq + "\r\n" +
      "Session: " + session.id
    Session: (stream) ->
      {
        id: Math.round(1000 + Math.random() * 8999).toString(),
        stream: stream
      }
  }
module.exports = rtsp()